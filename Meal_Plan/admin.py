from django.contrib import admin

from Meal_Plan.models import MealPlan

class MealPlanAdmin(admin.ModelAdmin):
    pass


admin.site.register(MealPlan, MealPlanAdmin)
